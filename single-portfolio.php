
<?php  get_header();?> 

   <!-- Page Title
   ================================================== -->
   <div id="page-title">

      <div class="row">

         <div class="ten columns centered text-center">
            <h1>Our Amazing Worksa<span>.</span></h1>

            <p>Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi
            enim tellus ultrices elit, amet consequat enim elit noneas sit amet luctu. </p>
         </div>

      </div>

   </div> <!-- Page Title End-->

   <!-- Content
   ================================================== -->
   <div class="content-outer">

      <div id="page-content" class="row portfolio">

         <section class="entry cf">

            <div id="secondary"  class="four columns entry-details">
               <?php 
               if( have_posts() ) { 

                 while( have_posts() ) { the_post(); ?>

                   <h1><?php the_title(); ?></h1>

                  <div class="entry-description">

                     <p><?php the_excerpt(); ?></p>

                  </div>

                  <ul class="portfolio-meta-list">
                     <li><span>Date: </span><?php the_field('project-date')?></li>
                     <li><span>Client </span><?php the_field('client');?></li>
                     <li><span>Skills: </span><?php the_terms( get_the_id(), 'skills', '', '/', '' ); ?></li>
                  </ul>

                  <a class="button" href="<?php the_permalink(); ?>">View project</a>  

            </div> <!-- secondary End-->

            <div id="primary" class="eight columns">

               <div class="entry-media">

                  <img src="images/portfolio/entries/geometric-backgrounds-01.jpg" alt="" />

                  <img src="images/portfolio/entries/geometric-backgrounds-02.jpg" alt="" />

               </div>

               <div class="entry-excerpt">

                  <p><?php the_content(); ?></p>

                  <?php the_post_thumbnail();?>

               </div>


            </div> <!-- primary end-->
             <div id="contact-form">

                  <!-- form -->
                  <form name="contactForm" id="contactFormPortfolio" method="post" action="<?php echo admin_url('admin-ajax.php?action=send_order_portfolio') ?>">
                     <fieldset>

                        <div class="half">
                           <label for="contactNamePortfolio">Name <span class="required">*</span></label>
                           <input name="contactNamePortfolio" type="text" id="contactName" size="35" value="" />
                        </div>

                        <div class="half pull-right">
                           <label for="contactEmailPortfolio">Email <span class="required">*</span></label>
                           <input name="contactEmailPortfolio" type="text" id="contactEmail" size="35" value="" />
                        </div>

                        <div>
                           <label for="contactPhonePortfolio">Phone</label>
                           <input name="contactPhonePortfolio" type="text" id="contactPhone"  value="" />
                        </div>

                        <div>
                           <input name="id_page" type="hidden" id="id_page" value="<?php echo get_the_ID();?>" />
                        </div>

                        <div>
                           <button class="submit">Хочу такую работу</button>
                           <span id="image-loader">
                              <img src="images/loader.gif" alt="" />
                           </span>
                        </div>

                     </fieldset>
                  </form> <!-- Form End -->

               </div>
             <?php }

                 wp_reset_query();

               } 
              
            ?>
          
         </section> <!-- end section -->

         <ul class="post-nav cf">
            <li class="prev"><a href="#" rel="prev"><strong>Previous Entry</strong> Duis Sed Odio Sit Amet Nibh Vulputate</a></li>
            <li class="next"><a href="#" rel="next"><strong>Next Entry</strong> Morbi Elit Consequat Ipsum</a></li>
         </ul>

      </div>

   </div> <!-- content End-->

   <!-- Tweets Section
   ================================================== -->
   <section id="tweets">

      <div class="row">

         <div class="tweeter-icon align-center">
            <i class="fa fa-twitter"></i>
         </div>

         <ul id="twitter" class="align-center">
            <li>
               <span>
               This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
               Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum
               <a href="#">http://t.co/CGIrdxIlI3</a>
               </span>
               <b><a href="#">2 Days Ago</a></b>
            </li>
         </ul>

         <p class="align-center"><a href="#" class="button">Follow us</a></p>

      </div>

   </section> <!-- Tweet Section End-->

   <!-- footer
   ================================================== -->
<?php  get_footer();?> 