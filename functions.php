<?php 
add_action('wp_enqueue_scripts', 'style_theme');
add_action('wp_enqueue_scripts', 'script_theme');
add_action('after_setup_theme', 'topMenu');
add_action('widgets_init', 'register_my_widgets' );
add_action('out_my_posts', 'outputOfPosts');
add_action('init', 'my_custom_post_type');
add_action( 'init', 'create_taxonomy' );

add_action( 'wp_ajax_send_mail', 'send_mail' );
add_action( 'wp_ajax_nopriv_send_mail', 'send_mail' );

add_filter('the_content', 'modify_conten');
add_shortcode('outputPosts', 'outputShortPosts');

function send_mail() 
{
	$contactName    = $_POST['contactName'];
	$contactEmail   = $_POST['contactEmail'];
	$contactSubject = $_POST['contactSubject'];
	$contactMessage = $_POST['contactMessage'];
	$to = get_option('admin_email');

	$headers = array(
	'From: Me Myself <me@example.net>',
	'content-type: text/html',
	'Cc: John Q Codex <jqc@wordpress.org>',
	'Cc: iluvwp@wordpress.org', // тут можно использовать только простой email адрес
);

	wp_mail( $to, $contactSubject, $contactMessage, $headers );
	wp_die();
}

function send_order_portfolio() 
{

	$contactNamePortfolio    = $_POST['contactNamePortfolio'];
	$contactEmailPortfolio   = $_POST['contactEmailPortfolio'];
	$contactNamePortfolio    = $_POST['contactPhonePortfolio'];
	$contactSubject 		 = "Хочу такую же работу!";
	$id_page    			 = $_POST['id_page'];
	$contactMessage 		 = "Новый заказ от ".$contactNamePortfolio."<Телефон >".$contactNamePortfolio.$contactEmailPortfolio;


	$to = get_option('admin_email');

	$headers = array(
	'From: Me Myself <me@example.net>',
	'content-type: text/html',
	'Cc: John Q Codex <jqc@wordpress.org>',
	'Cc: iluvwp@wordpress.org', // тут можно использовать только простой email адрес
);

	wp_mail( $to, $contactSubject, $contactMessage, $headers );
	wp_die();
}


function create_taxonomy(){

	// список параметров: wp-kama.ru/function/get_taxonomy_labels
	register_taxonomy( 'skills', [ 'portfolio' ], [
		'label'                 => '', // определяется параметром $labels->name
		'labels'                => [
			'name'              => 'Навыки',
			'singular_name'     => 'Навык',
			'search_items'      => 'Найти Навык',
			'all_items'         => 'Все Навыки',
			'view_item '        => 'Смотреть Навык',
			'parent_item'       => 'Родительский Навык',
			'parent_item_colon' => 'Родительский Навык:',
			'edit_item'         => 'Редактировать Навык',
			'update_item'       => 'Обновить Навык',
			'add_new_item'      => 'Добавить Новый Навык',
			'new_item_name'     => 'Новый Навык ',
			'menu_name'         => 'Навык',
		],
		'description'           => 'Навыки которые использовались в проекте', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null,
		'hierarchical'          => false,
		'rewrite'               => true,
	]);		
}

function my_custom_post_type() 
{
	register_post_type('portfolio', array(
		'labels'             => array(
			'name'               => 'Портфолио', // Основное название типа записи
			'singular_name'      => 'Портфолио', // отдельное название записи типа Book
			'add_new'            => 'Добавить работу',
			'add_new_item'       => 'Добавить новую работу',
			'edit_item'          => 'Редактировать работу',
			'new_item'           => 'Новая работа',
			'view_item'          => 'Посмотреть работу',
			'search_items'       => 'Найти работу',
			'not_found'          => 'Портфолио не найдено',
			'not_found_in_trash' => 'В корзине работ не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Портфолио'

		  ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      =>  4,
		'menu_icon'			 => 'dashicons-images-alt',
		'taxonomies'		 => array('skills'),
		'supports'           => array('title','editor','author','thumbnail','excerpt','comments')
	) );
}


function style_theme()
{
	wp_enqueue_style('style', get_stylesheet_uri());
	wp_enqueue_style( 'main_style', get_template_directory_uri() . '/css/default.css' );
	wp_enqueue_style( 'layout', get_template_directory_uri() . '/css/layout.css' );
	wp_enqueue_style( 'media', get_template_directory_uri() . '/css/media-queries.css' );

}

function script_theme()
{
	wp_deregister_script('jquery');
	wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js');
	wp_enqueue_script('jquery');
	wp_enqueue_script('ajax-form', get_template_directory_uri() . '/js/ajax-form.js', ['jquery'], 'true');
	wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', ['jquery'], null, true );
	wp_enqueue_script('doubletaptogo', get_template_directory_uri() . '/js/doubletaptogo.js', ['jquery'], null, true  );
	wp_enqueue_script('init', get_template_directory_uri() . '/js/init.js', ['jquery'], null, true  );
	wp_enqueue_script('modernizr', get_template_directory_uri(). '/js/modernizr.js', null, null, false);
}

function outputOfPosts() 
{
 	$arg = array(
            'numberposts' => 3,
            'order'     => 'ASC',
            'post_type'   => 'post',
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
         );
         
  	$posts = get_posts($arg);
}

function modify_conten($content) 
{
	$content = $content."Спасибо за просмотр";
	return $content;
}

function register_my_widgets()
{

	register_sidebar( array(
		'name'          => "Right sidebar",
		'id'            => "right_sidebar",
		'description'   => 'description sidebar',
		'class'         => '',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h5 class="widgettitle">',
		'after_title'   => "</h5>\n"
	) ); 
}


function topMenu() 
{
	register_nav_menu('top', 'Menu in the header');
	add_theme_support( 'post-thumbnails', array( 'post', 'portfolio' ) ); 
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'video' ) ); 
	add_image_size( 'post-thumb', 1300, 500, true );
	add_filter( 'excerpt_more', 'new_excerpt_more' );
	add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
	
	function new_excerpt_more( $more )
	{
		global $post;
		return '<a href="'. get_permalink($post) . '">Читать дальше...</a>';

	}

	
	function my_navigation_template( $template, $class )
	{

		return '
		<nav class="navigation %1$s" role="navigation">
			<div class="nav-links">%3$s</div>
		</nav>    
		';
	}

}

function outputShortPosts() 
{

	$atts = shortcode_atts( array(
	 'numberposts' => '3', 
	 'order'     => 'ASC',   
	), $atts );

	ob_start(); // start buffer;

 	global $post;

	$posts = get_posts($atts);
  
    foreach( $posts as $post ) {  ?>

        <p>
        	<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
        </p>
      
    <?php } 

    wp_reset_postdata(); // сброс

	$output = ob_get_clean();

    return $output;
}

