
<aside id="sidebar">

   <h5>Last posts</h5>

   <?php  do_action('out_my_posts'); ?>

   <?php 
      foreach( $posts as $post ) {  ?>

         <p>
            <a href="<?php the_permalink();?>"><?php the_title(); ?></a>
         </p>
      
      <?php } ?> 
  
   <?php dynamic_sidebar('right_sidebar'); ?>

</aside>
