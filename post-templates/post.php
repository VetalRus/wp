 <article class="post">

   <div class="entry-header cf">

      <h1><?php the_title(); ?></h1>

   </div>

   <div class="post-thumb">
      <?php the_post_thumbnail('post_thumb'); ?>
   </div>

   <div class="post-content">
      <p class="lead"><?php the_content();?></p>
   </div>

</article>